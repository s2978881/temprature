package nl.utwente.di.temperature;

public class Converter {
    public double convert(String Celsius) {

        double CelsiusDouble;
        try {
            CelsiusDouble = Double.parseDouble(Celsius);
        } catch (NumberFormatException e) {
            try {
                CelsiusDouble = Double.parseDouble(Celsius.replace(',', '.'));
            } catch (NumberFormatException e2) {
                try {
                    CelsiusDouble = Integer.parseInt(Celsius);
                } catch (NumberFormatException e3) {
                    return 0.0;
                }
            }
        }

        return (CelsiusDouble * 9 / 5) + 32;

    }
}
